﻿Imports System.Text.RegularExpressions

Partial Public Class UI

	Public Class __Regex
		Private Const ExpressionOptions As RegexOptions = RegexOptions.Singleline Or RegexOptions.Multiline Or RegexOptions.IgnoreCase

		Public Shared Function Trim(This As String) As String
			Dim tagRemove As Regex = New Regex("<[^>]*(>|$)")
			Dim result As String = tagRemove.Replace(This, String.Empty)
			Return result
		End Function
		Public Shared Function GetTag(This As String, Tag As String) As String
			Dim regex As New Regex($"<\s*{Tag}[^>]*>(.*?)<\s*/\s*{Tag}>", ExpressionOptions)
			Dim results As New Text.StringBuilder
			For Each m As Match In regex.Matches(This)
				results.Append(m.Value)
			Next
			Return results.ToString()
		End Function
		Public Shared Function GetTags(This As String, Tag As String) As List(Of String)
			Dim CleanThis = UI.__Regex.GetTag(This, Tag)
			Dim results As New List(Of String)
			Dim _reg As New Regex($"<{Tag}>(?<data>[\s\S]*?)<\/{Tag}>", ExpressionOptions)
			Dim doregex2 As MatchCollection = _reg.Matches(CleanThis)
			For Each item As Match In doregex2
				results.Add(item.Value)
			Next
			Return results
		End Function

		Public Shared Function Create(HTML As String) As ResultadoEN
			Dim _HTML = UI.ValueTrimFull(HTML)
			Dim Resultado As New ResultadoEN
			Try
				Dim lstTRs = UI.__Regex.GetTags(_HTML, "tr")
				If lstTRs IsNot Nothing AndAlso lstTRs.Count > 1 Then
					For i = 1 To lstTRs.Count - 1
						Dim item = lstTRs(i)
						Dim lstTDs = UI.__Regex.GetTags(item, "td")
						Dim objTemp As New TipoCambioEN
						objTemp.fecha = UI.ValueTrimFull(lstTDs(0), True)
						objTemp.moneda_descripcion = UI.ValueTrimFull(lstTDs(1), True)
						objTemp.precio_compra = UI.ValueTrimFull(lstTDs(2), True)
						objTemp.precio_venta = UI.ValueTrimFull(lstTDs(3), True)
						Resultado.tipo_cambio.Add(objTemp)
					Next
					Resultado.success = True
				End If
			Catch ex As Exception
				Resultado.mensaje = ex.Message
			End Try
			Return Resultado
		End Function
	End Class
End Class
