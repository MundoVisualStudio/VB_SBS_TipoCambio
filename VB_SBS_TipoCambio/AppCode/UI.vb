﻿Imports System.Text.RegularExpressions

Partial Public Class UI
	Public Shared Function Value(Valor As Object, Optional DefaultValue As String = "") As String
		Dim NuevoValor As String
		Try
			NuevoValor = Convert.ToString(Valor)
			If Valor Is "" OrElse Valor Is Nothing OrElse Valor Is DBNull.Value Then
				NuevoValor = DefaultValue
			End If
		Catch ex As Exception
			NuevoValor = DefaultValue
		End Try

		Return NuevoValor
	End Function

	Public Shared Function ValueTrim(ByVal Valor As Object, ByVal Optional DefaultValue As String = "") As String
		Dim NuevoValor As String = UI.Value(Valor, DefaultValue)
		NuevoValor = NuevoValor.Trim()
		Return NuevoValor
	End Function
	Public Shared Function ValueTrimFull(node As String, Optional cleartags As Boolean = False) As String

		If cleartags Then
			node = UI.__Regex.Trim(node)
		End If

		node = UI.ValueTrim(node)

		node = node.Replace(vbCrLf, "")
		node = node.Replace(vbLf, "")
		node = node.Replace(vbTab, "")
		node = node.Replace(New String(" "c, 10), " ")
		node = node.Replace(New String(" "c, 9), " ")
		node = node.Replace(New String(" "c, 8), " ")
		node = node.Replace(New String(" "c, 7), " ")
		node = node.Replace(New String(" "c, 6), " ")
		node = node.Replace(New String(" "c, 5), " ")
		node = node.Replace(New String(" "c, 4), " ")
		node = node.Replace(New String(" "c, 3), " ")
		node = node.Replace(New String(" "c, 2), " ")
		'node = node.Replace("  ", "")
		'Node = Regex.Replace(Node, "[^\w\.@-]", String.Empty)
		Return node
	End Function

End Class
