﻿Imports System.Net.Http

Partial Public Class UI
	Public Class SBS
		Public Shared Async Function TipoDeCambio(fecha_inicio As String, Optional fecha_fin As String = "") As Task(Of ResultadoEN)
			Dim resultado As New ResultadoEN
			Try
				If String.IsNullOrEmpty(fecha_fin) Or String.IsNullOrWhiteSpace(fecha_fin) Then
					fecha_fin = fecha_inicio
				End If
				Dim URL = $"http://www.sbs.gob.pe/app/stats/seriesH-tipo_cambio_moneda_excel.asp?fecha1={fecha_inicio}&fecha2={fecha_fin}&moneda=02"
				Dim cookies As New Net.CookieContainer()
				Dim DATA = ""
				Dim client As HttpClient = New HttpClient(New HttpClientHandler() With {.CookieContainer = cookies, .UseCookies = True, .AllowAutoRedirect = True})
				Dim response As HttpResponseMessage = Await client.GetAsync(URL)
				If response.IsSuccessStatusCode Then
					DATA = Await response.Content.ReadAsStringAsync()
					DATA = DATA.Replace("�", "ó")
					resultado = UI.__Regex.Create(DATA)
				End If
			Catch ex As Exception
				resultado.mensaje = ex.Message
			End Try
			Return resultado
		End Function
	End Class
End Class