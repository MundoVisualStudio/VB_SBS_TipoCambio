﻿Public Class ResultadoEN
	Public Property success As Boolean = False
	Public Property mensaje As String = ""
	Public Property tipo_cambio As New List(Of TipoCambioEN)
	Public Function AsJson() As String
		Dim resultado As String
		Try
			Dim serializador As New System.Web.Script.Serialization.JavaScriptSerializer
			Dim sb As New System.Text.StringBuilder
			resultado = serializador.Serialize(Me)
		Catch ex As Exception
			resultado = ex.Message
		End Try
		Return resultado
	End Function
End Class